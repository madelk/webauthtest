var http = require('http');
var path = require('path');
var crypto = require('crypto');
var express = require('express');

var router = express();
var server = http.createServer(router);

router.use(express.static(path.resolve(__dirname, 'client')));

var users = [{
  pub: 2345678,
  sec: 412512,
  username: 'test'
}];


function getExpireDateTime(){
  var now = new Date();
  now.setMinutes ( now.getMinutes() + 60 );
  return now;
}

// /getToken?key=2345678&sig=3d737762795be38a5ebfd3c1cab01a57
router.get('/getToken', GetAuth);

function GetAuth(req, res, next) {
  var key = req.query.key;
  var sig = req.query.sig;
  if (!key) {
    res.status(400).send('No Key sent');
    return;
  }
  else if (!sig) {
    res.status(400).send('No Signature sent');
    return;
  }

  var user = users.filter(function(item) {
    return item.pub == key;
  });

  if (user.length == 0) {
    res.status(400).send('Invalid Key');
    return;
  }
  user = user[0];
  var validSig = crypto.createHash('md5').update(user.sec + "key=" + user.pub).digest("hex");
  if (sig != validSig) {
    res.status(400).send('Invalid Signature');
    return;
  }
  var token = "";
  if (typeof (user.tokenExpire) === "undefined" || new Date() > user.tokenExpire){
    token = crypto.createHash('md5').update((new Date()).getTime().toString() + user.pub + user.key).digest("hex");
    user.token = token;
    user.tokenExpire = getExpireDateTime();
  } else {
    token = user.token;
  }
  
  res.status(200).send(token);
}

function auth(req, res, next) {
  var key = req.query.key;
  var sig = req.query.sig;
  var id = req.query.id;
  var token = req.query.token;
  if (!key) {
    res.status(400).send('No Key sent');
    return;
  }
  else if (!sig) {
    res.status(400).send('No Signature sent');
    return;
  }
  else if (!id) {
    res.status(400).send('No ID sent');
    return;
  }

  else if (!token) {
    res.status(400).send('No token sent');
    return;
  }

  var user = users.filter(function(item) {
    return item.pub == key;
  });

  if (user.length == 0) {
    res.status(400).send('Invalid Key');
    return;
  }
  user = user[0];
  var validSig = crypto.createHash('md5').update(user.sec + "key=" + user.pub + "&id=" + id + "&token=" + token).digest("hex");
  if (sig != validSig) {
    res.status(400).send('Invalid Signature');
    //res.status(400).send('Invalid Signature: ' + sig +' should have been ' + validSig);
    return;
  }
  next();
}

function getData(req, res, next) {
  res.status(200).send("A winner is you!! I'd show you data for websiteid " + req.query.id + " if only I'd written that code");
}

function getOtherData(req, res, next) {
  res.status(200).send("A winner is you!! I'd show you Other data for websiteid " + req.query.id + " if only I'd written that code");
}

// getData?key=2345678&id=24601&token=cb444effb1b45a10d3d9d58561c86c31&sig=091915104e404251a4b2618db7d920fa 
router.get('/getData', auth, getData);


server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
  var addr = server.address();
  console.log("Server listening at", addr.address + ":" + addr.port);
});
